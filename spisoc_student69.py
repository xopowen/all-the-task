empty_student = {'name': '', 
                 'second_name': '',
                 'group': 0,
                 'age': 0,
                 'year_final': 0,
                 'salary': 0
                }
print_order = ['name', 'second_name','group', 'age', 'year_final', 'salary' ]

students = []

def add_student(*kwargs):
    if kwargs is not None:        
        students.append(empty_student.copy())
        current_student = students[-1]      
        for key in current_student:            
            current_student[key] = input('Enter '+ key + ':')
            if type(empty_student[key]) == type(1):
                current_student[key] = int(current_student[key])

                

def remove_student():
    print('Remove student')
    name = input('Enter name for deletion:')
    second_name = input('Enter second name for deletion:')
    age = input('Enter age for deletion:')
    found_student = None
    for student in students:
        if (name == student['name'] and
            second_name == student['second_name'] and
            age == str(student['age'])):
            found_student = student
            break
    if found_student != None:
        students.remove(found_student)
    pass

def dump_student_database():
    f = open('students.db', 'w')
    f.write(str(students))
    f.close()


def load_student_database():
    f = open('students.db', 'r')
    global students
    students = eval(f.readline())   
    f.close()
    
def print_students():
    print('Database has ' + str(len(students)) + ' students')
    for student in students:
        print('---------------')
        for key in print_order:
            pad =  ' ' * (len('second_name') - len(key))
            print(key, pad, student[key])

#load_student_database()
second_names=open('names.txt','r').read().splitlines()
names=open('names.txt','r').read().splitlines()
def seed_db_automatically_student (*kwargs):
    import time
    import random 
    start=time.time()
    if kwargs is not None:
        students.append(empty_student.copy())
        current_student = students[-1]
      
        while len(students)  <= 10000:
            for key in current_student:
                if key == 'name':              
                    for name in names:
                        for second_name in second_names :
                            if name != second_name:
                                current_student['name']= name                           
                                current_student['second_name']= second_name 
                                current_student['group']=random.randint(1000,5000)
                                current_student['age'] =random.randint(17,30)
                                current_student['year_final'] =random.randint(2019,2025)
                                current_student['salary'] = random.randint(1000,10000)
                                students.append(current_student.copy())
                                
    f = open('students.db', 'w')
    f.write(str(students))
    f.close()
    print(time.time()-start)
    
def red(filename):
    f = open(filename,'r')
    content =[line for line in f.readlines()]
    f.close()
    
while True:
    regime = input('Enter mode (1 - add, 2 - remove, 3 - printout, 4 - ):')
    if regime == '1':          
        add_student()
        dump_student_database()
    elif regime == '2':
        remove_student()
        dump_student_database()
    elif regime == '3':
        print_students()
    elif regime == '4':
        seed_db_automatically_student()
        
exit()

                            
